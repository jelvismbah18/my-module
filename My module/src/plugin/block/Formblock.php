<?php

namespace Drupal\My module\Plugin\Block;

use Drupal\Core\Block\BlockBase;

use Drupal\Core\Block\BlockPluginInterface;

use Drupal\Core\Form\FormStateInterface;

class Formblock extends BlockBase implements BlockPluginInterface {

  public function build() {

    return array(

      '#markup' => $this->t('Welcome!'),
    );
  }

}
  public function blockForm($form, FormStateInterface $form_state) {

    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['Username'] = [

    '#type' => 'textfield',

    '#title' => $this->t('Username'),

    '#description' => $this->t('Input Your name'),

    '#default_value' => isset($config['Username']) ? $config['Username'] : '',

    ];

    return $form;

  }

  public function blockSubmit($form, FormStateInterface $form_state) {

    parent::blockSubmit($form, $form_state);

    $values = $form_state->getValues();

    $this->configuration['Username'] = $values[''];

  }

  public function custom_submit_form($form, FormStateInterface $form_state) {

    $values = $form_state->getValues();

  
      $form['actions']['custom_submit'] = [

      '#type' => 'submit',

      '#name' => 'custom_submit',

      '#value' => $this->t('Submit'),

      '#submit' => array('::custom_submit_form'),
    ];

      return $form;

  }